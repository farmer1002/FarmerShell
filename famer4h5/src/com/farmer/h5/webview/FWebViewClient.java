package com.farmer.h5.webview;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class FWebViewClient extends WebViewClient
{
    private ClickTranfer cTranfer;
    
    interface ClickTranfer
    {
        void loadListener(WebView view, String url);
        
        void finishListener(WebView view, String url);
        
        void startListener(WebView view, String url, Bitmap favicon);
        
        void receivedErrorListener(WebView view, int errorCode, String description, String failingUrl);
    }

    public void setcTranfer(ClickTranfer cTranfer)
    {
        this.cTranfer = cTranfer;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url)
    {
        if (null != cTranfer)
        {
            cTranfer.loadListener(view, url);
        }
        else
        {
            view.loadUrl(url);
        }
        return true;
    }
    
    @Override
    public void onPageFinished(WebView view, String url)
    {
        super.onPageFinished(view, url);
        if (null != cTranfer)
        {
            cTranfer.finishListener(view, url);
        }
    }
    
    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon)
    {
        super.onPageStarted(view, url, favicon);
        if (null != cTranfer)
        {
            cTranfer.startListener(view, url, favicon);
        }
    }
    
    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
    {
        super.onReceivedError(view, errorCode, description, failingUrl);
        if (null != cTranfer)
        {
            cTranfer.receivedErrorListener(view, errorCode, description, failingUrl);
        }
    }
}

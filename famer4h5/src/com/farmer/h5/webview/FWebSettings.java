package com.farmer.h5.webview;

public class FWebSettings
{
    /**
     * 是否可以放大\缩小
     */
    public boolean isZoom;
    
    /**
     * 是否显示放大\缩小控件
     */
    public boolean isShowZoomControll;
    
    /**
     * 是否使用缓存
     */
    public boolean isUseCache;
    
    /**
     * 是否使用GPS定位
     */
    public boolean isUseGps;
    
    /**
     * 是否可以点击物理返回键
     */
    public boolean isCanPressBack;
    
    /**
     * UserAgent
     */
    public String userAgent;
    
    /**
     * 错误画面
     */
    public String errorHtml;
    
    /**
     * 是否显示加载进度条
     */
    public boolean isShowLoading;
    
    /**
     * 是否清除cookies
     */
    public boolean isCleanCookies;
}

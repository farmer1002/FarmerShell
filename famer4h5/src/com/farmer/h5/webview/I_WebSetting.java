package com.farmer.h5.webview;

public interface I_WebSetting
{
    /**
     * 设置webView的WebSetting
     */
    void setWebViewSetting();
    
    /**
     * 设置webView的userAgent
     */
    void setWebViewUserAgent();
    
    /**
     * 设置webView的ChromClient
     */
    void setWebViewChromClient();
    
    /**
     * 设置webView的WebClient
     */
    void setWebClient();
}

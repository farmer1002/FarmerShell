package com.farmer.h5.util;

import java.util.Properties;

import com.farmer.h5.webview.FWebSettings;

import android.content.Context;

/**
 * 配置文件工具类
 * @author zhu_qiang1
 *
 */
public class PropertyUtil
{
    public static FWebSettings getPropertiesURL(Context c)
    {
        Properties properties = new Properties();
        FWebSettings setting = new FWebSettings();
        try
        {
            properties.load(c.getAssets().open("web.properties"));
            // 是否可以放大\缩小
            setting.isZoom = Boolean.parseBoolean(properties.getProperty("isZoom"));
            // 是否显示放大\缩小控件
            setting.isShowZoomControll = Boolean.parseBoolean(properties.getProperty("isShowZoomControll"));
            // 是否使用缓存
            setting.isUseCache = Boolean.parseBoolean(properties.getProperty("isUseCache"));
            // 是否使用GPS定位
            setting.isUseGps = Boolean.parseBoolean(properties.getProperty("isUseGps"));
            // 是否可以点击物理返回键
            setting.isCanPressBack = Boolean.parseBoolean(properties.getProperty("isCanPressBack"));
            // UserAgent
            setting.userAgent = properties.getProperty("userAgent");
            // 错误画面
            setting.errorHtml = properties.getProperty("errorHtml");
            // 是否显示加载进度条
            setting.isShowLoading = Boolean.parseBoolean(properties.getProperty("isShowLoading"));
            // 是否显示加载进度条
            setting.isCleanCookies = Boolean.parseBoolean(properties.getProperty("isCleanCookies"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return setting;
    }
}

package com.farmer.h5.util;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.famer.h5.R;

/**
 * 自定义提示Toast
 * 
 * @author zhu_qiang1
 */
public class TipsToast extends Toast
{
    
    public TipsToast(Context context)
    {
        super(context);
    }
    
    public static TipsToast makeText(Context context, CharSequence text, int duration)
    {
        TipsToast result = new TipsToast(context);
        
        LayoutInflater inflate = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(R.layout.view_tip, null);
        TextView tv = (TextView)v.findViewById(R.id.tips_msg);
        tv.setText(text);
        
        result.setView(v);
        // setGravity方法用于设置位置，此处为垂直居中
        // result.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        result.setDuration(duration);
        
        return result;
    }
    
    public static TipsToast makeText(Context context, int resId, int duration)
        throws Resources.NotFoundException
    {
        return makeText(context, context.getResources().getText(resId), duration);
    }
    
    public void setIcon(int iconResId)
    {
        if (getView() == null)
        {
            throw new RuntimeException("This Toast was not created with Toast.makeText()");
        }
        ImageView iv = (ImageView)getView().findViewById(R.id.tips_icon);
        if (iv == null)
        {
            throw new RuntimeException("This Toast was not created with Toast.makeText()");
        }
        if (0 != iconResId)
        {
            iv.setVisibility(View.VISIBLE);
            iv.setImageResource(iconResId);
        }
        else
        {
            iv.setVisibility(View.GONE);
        }
    }
    
    @Override
    public void setText(CharSequence s)
    {
        if (getView() == null)
        {
            throw new RuntimeException("This Toast was not created with Toast.makeText()");
        }
        TextView tv = (TextView)getView().findViewById(R.id.tips_msg);
        if (tv == null)
        {
            throw new RuntimeException("This Toast was not created with Toast.makeText()");
        }
        tv.setText(s);
    }
    
    public void showTips(TipsToast tipsToast, Context context, int iconResId, int msgResId)
    {
        if (tipsToast != null)
        {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
            {
                tipsToast.cancel();
            }
        }
        else
        {
            tipsToast = TipsToast.makeText(context, msgResId, TipsToast.LENGTH_SHORT);
        }
        tipsToast.show();
        tipsToast.setIcon(iconResId);
        tipsToast.setText(msgResId);
    }
    
    public void showTips(TipsToast tipsToast, Context context, int iconResId, String msg)
    {
        if (tipsToast != null)
        {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
            {
                tipsToast.cancel();
            }
        }
        else
        {
            tipsToast = TipsToast.makeText(context, msg, TipsToast.LENGTH_SHORT);
        }
        tipsToast.show();
        tipsToast.setIcon(iconResId);
        tipsToast.setText(msg);
    }
}

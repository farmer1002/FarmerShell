package com.farmer.h5.test;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.ValueCallback;

import com.famer.h5.R;
import com.farmer.h5.util.PropertyUtil;
import com.farmer.h5.util.WebViewCacheUtil;
import com.farmer.h5.webview.FWebChromeClient;
import com.farmer.h5.webview.FWebSettings;

public class MainActivity extends Activity
{
    private MyFarmerWebView my_webview;
    
    private FWebSettings fwebSettings;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        
        fwebSettings = PropertyUtil.getPropertiesURL(this);
        my_webview = (MyFarmerWebView)findViewById(R.id.my_webview);
        my_webview.loadUrl("http://www.baidu.com");
        
        if (!fwebSettings.isUseCache)
        {
            // 清除缓存
            WebViewCacheUtil.clearCacheFolder(this.getCacheDir(), System.currentTimeMillis());
            WebViewCacheUtil.clearCache(my_webview);
        }
        
        if (fwebSettings.isCleanCookies)
        {
            WebViewCacheUtil.clearCookies(this);
        }
    }
    
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        
        if (!fwebSettings.isUseCache)
        {
            // 清除缓存
            WebViewCacheUtil.clearCacheFolder(this.getCacheDir(), System.currentTimeMillis());
            WebViewCacheUtil.clearCache(my_webview);
            WebViewCacheUtil.clear4Exit(this);
        }
        
        if (fwebSettings.isCleanCookies)
        {
            WebViewCacheUtil.clearCookies(this);
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == FWebChromeClient.FILECHOOSER_RESULTCODE)
        {
            ValueCallback<Uri> mUploadMessage = my_webview.getWebViewChromClient().getValueCallback();
            if (null == mUploadMessage)
            {
                return;
            }
            Uri result = intent == null || resultCode != Activity.RESULT_OK ? null : intent.getData();
            // 用完以后 需要释放ValueCallback 不然还有不能重复选择的问题
            mUploadMessage.onReceiveValue(result);
            mUploadMessage = null;
        }
    }
}

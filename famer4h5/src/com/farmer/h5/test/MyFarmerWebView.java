package com.farmer.h5.test;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

import com.farmer.h5.webview.FarmerWebView;

public class MyFarmerWebView extends FarmerWebView
{
    public MyFarmerWebView(Context context)
    {
        super(context);
    }
    
    public MyFarmerWebView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    
    @Override
    @SuppressLint("SetJavaScriptEnabled")
    public void setWebViewSetting()
    {
        super.setWebViewSetting();
        
        //webSettings.getAllowContentAccess();
    }
    
    @Override
    public void setWebViewChromClient()
    {
        super.setWebViewChromClient();
    }
    
    @Override
    public void setWebClient()
    {
        super.setWebClient();
    }
    
    @Override
    public void setWebViewUserAgent()
    {
        super.setWebViewUserAgent();
    }
    
    @Override
    public void floadListener(String url)
    {
        super.floadListener(url);
    }
    
    @Override
    public void ffinishListener(String url)
    {
        super.ffinishListener(url);
    }
    
    @Override
    public void fstartListener(String url)
    {
        super.fstartListener(url);
    }
     
    @Override
    public void freceivedErrorListener(WebView view, int errorCode, String description, String failingUrl)
    {
        super.freceivedErrorListener(view, errorCode, description, failingUrl);
    }
}
